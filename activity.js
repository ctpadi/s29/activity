const express = require("express");
const app = express();
const port = 4000;

app.use(express.json());

// Mock Database
let users = [
	{
		username: "hatdog",
		password: "masarap"
	},
	{
		username: "itlog",
		password: "umay"
	},
	{
		username: "tocino",
		password: "diabetes"
	},
	{
		username: "maling",
		password: "yuck"
	},
	{
		username: "spam",
		password: "yehey"
	},
	{
		username: "hotel",
		password: "trivago"
	}
];

app.get('/home',(req,res)=>{

	res.send("Welcome to the home page");

})


app.get("/users", (req, res) => {
	res.send(users);
});

app.delete("/delete-user", (req, res) => {
	let index = null;
	for(let i = 0; i < users.length; i++){
		if (req.body.username === users[i].username){
			index = i;
			break;
		}
	}
	if (index !== null){
		users.splice(index, 1);
		res.send(`User ${req.body.username} has been deleted.`)
	} 
	else{
		res.send(`User ${req.body.username} not found.`)
	}
	console.log(users);
});


app.listen(port,()=>console.log(`Server is running at port ${port}`));